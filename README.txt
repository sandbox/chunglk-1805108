Usage
  1. Setup module
  2. Configuration realname (admin/config/people/realname)
  3. Add an entity reference field, choose autocomplete and 
     set target type "user", set using "Real name"
